# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.1] - 2024-07-23

### Changed

- Updated to work with Librecast > v0.8.0

### Fixed

- Replace sigtimedwait(2) in tests. sigtimedwait(2) is missing on OpenBSD and some other systems.
- Fixed a number of compiler warnings on NetBSD 10.0
- ensure CFLAGS required for tests are preserved

## [0.3.0] - 2023-11-07

### Added

- -r / --recursive file and directory syncing
- --archive / -a (archive option handling for -a (-t -p -o -g -r))
- --hex supports directory hashes

### Changed

- split lcshare (server) and lcsync (client) programs

## [0.2.1] - 2023-08-08

### Added

- --version

### Changed

- updates for Librecast API 0.7.0 release

### Fixed
- install: install lcsync(1) man page
- ensure configure fails when libsodium not present

## [0.2.0] - 2023-08-01

This release adds symmetric key encryption.

### Added

- Symmetric encryption is provided  using  the  XSalsa20  stream  cipher  from
  libsodium  with Poly1305  MAC  authentication tags. A keyfile can be provided,
  or a key can be derived from a user-supplied password.

### Changed

- homogenised test runner with librecast library
- updated and re-enabled relevant tests

### Removed

- Obsolete tests. Most of the lcrq functionality is now provided by the
  librecast library. There is no need to duplicate those tests here.

## [0.1.0] - 2023-07-31

More or less a complete rewrite, with all merkle tree, indexing, syncing and
queuing moved into liblibrecast.

### Added
- RaptorQ (RFC 6330) encoding
- output basic sync stats on client
- --bwlimit option to ratelimit send
- multithreaded recv
- FreeBSD support
- autotools: configure script etc.
- man page

### Changed
- MLD triggering by default in server mode
- drop privileges in client mode
- using merkle tree, multicast indexing, syncing and queuing from Librecast API
- improved test runner

### Fixed
- various bugfixes. More or less a complete rewrite.
- install setuid when setcap not available (non-Linux)

### Removed
- MLD bloom filters & SIMD bloom timers.

## [0.0.1] - 2022-05-19

### Added
- this CHANGELOG

### Changed
- Makefile: Pass -ffile-prefix-map in CFLAGS to avoid embedding the build path.
- Makefile: remove unused dependencies
- Makefile: don't hardcode bash path
- updated COPYING: more explicit license wording
- fix SPDX in header files
- updated README: better description + document options

### Fixed
- various license headers
- applied patches for Debian packaging

## [0.0.0] - 2022-05-16

- Initial release.
