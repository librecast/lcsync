/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2023 Brett Sheffield <bacs@librecast.net> */

#include "config.h"
#include "globals.h"
#include "help.h"
#include "log.h"
#include <libgen.h>
#include <stdio.h>

void help_usage(void)
{
	WARN("usage: '%s source [destination]'\n", basename(progname));
}

void help_usage_hex(void)
{
	WARN("usage: '%s --hex filename'\n", basename(progname));
}

void help_version(void)
{
	INFO("%s\n", PACKAGE_STRING);
}
