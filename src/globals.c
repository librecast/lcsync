/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2023 Brett Sheffield <bacs@librecast.net> */

#include <stddef.h>
#include "globals.h"

/* global defaults */

int (*action)(int *argc, char *argv[]) = &succeed;
char *bwlimit;
char *iface = NULL;
char *keyfile = NULL;
char *password;
char *progname;
int archive = 0;
int batchmode = 0;
int dryrun = 0;
int hex = 0;
int loopback = 0;
int quiet = 0;
int recurse = 0;
int remote = 0;
int setgroup = 0;
int setowner = 0;
int setperms = 0;
int settimes = 0;
int verbose = 0;
int version = 0;
size_t bpslimit = 0;
unsigned char secretkey[crypto_pwhash_STRBYTES];
unsigned int ifx = 0;

int succeed(int *argc, char *argv[])
{
	(void) argc; (void) argv;
	return 0;
}
