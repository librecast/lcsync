/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2020-2023 Brett Sheffield <bacs@librecast.net> */

#ifndef _GLOBALS_H
#define _GLOBALS_H 1

#include <sodium.h>
#include <stdint.h>

#define THREAD_MAX 128
#define PASSWORD_MAX 1024

#ifndef EKEYREJECTED
# define    EKEYREJECTED    129
#endif

extern char *bwlimit;
extern char *iface;
extern char *keyfile;
extern char *password;
extern char *progname;
extern int (*action)(int *argc, char *argv[]);
extern int archive;
extern int batchmode;
extern int dryrun;
extern int hex;
extern int loopback;
extern int quiet;
extern int recurse;
extern int remote;
extern int setgroup;
extern int setowner;
extern int setperms;
extern int settimes;
extern int verbose;
extern int version;
extern size_t bpslimit;
extern unsigned char secretkey[crypto_pwhash_STRBYTES];
extern unsigned int ifx;

/* default action is to do nothing, successfully */
int succeed(int *argc, char *argv[]);
#endif /* _GLOBALS_h */
